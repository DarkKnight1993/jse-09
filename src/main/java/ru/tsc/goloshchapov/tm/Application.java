package ru.tsc.goloshchapov.tm;

import ru.tsc.goloshchapov.tm.api.ICommandController;
import ru.tsc.goloshchapov.tm.api.ICommandRepository;
import ru.tsc.goloshchapov.tm.api.ICommandService;
import ru.tsc.goloshchapov.tm.component.Bootstrap;
import ru.tsc.goloshchapov.tm.constant.ArgumentConst;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.controller.CommandController;
import ru.tsc.goloshchapov.tm.model.Command;
import ru.tsc.goloshchapov.tm.repository.CommandRepository;
import ru.tsc.goloshchapov.tm.service.CommandService;
import ru.tsc.goloshchapov.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}
