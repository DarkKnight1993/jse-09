package ru.tsc.goloshchapov.tm.service;

import ru.tsc.goloshchapov.tm.api.ICommandRepository;
import ru.tsc.goloshchapov.tm.api.ICommandService;
import ru.tsc.goloshchapov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
