package ru.tsc.goloshchapov.tm.api;

import ru.tsc.goloshchapov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
