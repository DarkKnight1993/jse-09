package ru.tsc.goloshchapov.tm.api;

public interface ICommandController {

    void exit();

    void showErrorCommand();

    void showErrorArgument();

    void showWelcome();

    void showEnterCommand();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

    void showCommandValue(String value);

    void showInfo();

}
